from django.contrib.gis import admin
from .models import Post

# Register your models here.
@admin.register(Post)
class ProfessionnelAdmin(admin.OSMGeoAdmin):
    list_display = ['tel', 'date', 'coord']
    exclude = []