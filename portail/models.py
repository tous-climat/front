from django.contrib.gis.db import models

# Create your models here.


class Post(models.Model):
    coord = models.PointField('coordonnées', srid=4326, blank=True, null=True)
    tel = models.CharField(max_length=12, verbose_name="N° de téléphone")
    date = models.DateTimeField('date de création', auto_now_add=True)

    def __str__(self):
        return self.tel
